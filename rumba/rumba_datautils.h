// Copyright 2022 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_RUMBA_DATAUTILS
#define INCLUDED_RUMBA_DATAUTILS

#include <sys/time.h>

#include <build/buildbox/local_execution.pb.h>

namespace rumba {

struct DataUtils {
    static const uint16_t DEFAULT_RUMBA_PORT;
    static void collectCompilationData(
        const int argc, char const *const *argv,
        const std::string unresolvedPathToCommand,
        build::buildbox::CompilerExecutionData &compilationData);
    static void
    sendData(const build::buildbox::CompilerExecutionData &compilationData);
};

/*
 * Parses the configuration provided by both the config file at [configPath]
 * and the environment variables set in the runtime environment. If
 * [configPath] is empty, the configuration is set by the environment alone.
 * When both are provided, environment variables take precedence over the
 * config to match recc.
 */
void parseConfig(std::string configPath);

// Global variables to be updated by environment variables or a config file
extern std::string RUMBA_LOG_DIRECTORY;
extern bool RUMBA_VERBOSE;
extern std::string RUMBA_LOG_LEVEL;
extern bool RUMBA_USE_RECC;
extern bool RUMBA_VERIFY;
extern std::string RUMBA_PORT;
extern std::string RUMBA_CORRELATED_INVOCATIONS_ID;

} // namespace rumba
#endif
