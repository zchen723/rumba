// Copyright 2022 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <atomic>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <vector>

#include <google/protobuf/util/time_util.h>

#include <buildbox/buildboxcommon_cashash.h>
#include <buildbox/buildboxcommon_executionstatsutils.h>
#include <buildbox/buildboxcommon_fileutils.h>
#include <buildbox/buildboxcommon_logging.h>
#include <buildbox/buildboxcommon_protojsonutils.h>
#include <buildbox/buildboxcommon_systemutils.h>
#include <buildbox/buildboxcommon_temporaryfile.h>
#include <buildbox/buildboxcommon_timeutils.h>

#include <recc/executioncontext.h>

#include <build/buildbox/local_execution.pb.h>
#include <rumba_datautils.h>

#define VERIFY_ERROR(x)                                                       \
    {                                                                         \
        std::stringstream _msg;                                               \
        _msg << x;                                                            \
        BUILDBOX_LOG_ERROR(_msg.str());                                       \
        std::cerr << _msg.str() << "\n";                                      \
    }

using namespace rumba;

namespace {

std::atomic_bool s_signalReceived(false);

/**
 * Signal handler to mark the remote execution task for cancellation
 */
void setSignalReceived(int) { s_signalReceived = true; }

void setupSignals()
{
    struct sigaction sa;
    sa.sa_handler = setSignalReceived;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    if (sigaction(SIGINT, &sa, nullptr) == -1) {
        std::cerr << "Unable to register signal handler for SIGINT"
                  << std::endl;
    }
    if (sigaction(SIGTERM, &sa, nullptr) == -1) {
        std::cerr << "Unable to register signal handler for SIGTERM"
                  << std::endl;
    }
    if (sigaction(SIGHUP, &sa, nullptr) == -1) {
        std::cerr << "Unable to register signal handler for SIGHUP"
                  << std::endl;
    }
}

void setupLoggerFromEnvironment(const char *programName)
{
    std::string logDirectory;
    if (!rumba::RUMBA_LOG_DIRECTORY.empty()) {
        logDirectory = RUMBA_LOG_DIRECTORY;
    }

    buildboxcommon::LogLevel logLevel;
    if (rumba::RUMBA_VERBOSE) {
        logLevel = buildboxcommon::LogLevel::DEBUG;
    }
    else {
        const auto &validLogLevels =
            buildboxcommon::logging::stringToLogLevelMap();
        auto logLevelIt = validLogLevels.find(rumba::RUMBA_LOG_LEVEL);
        if (logLevelIt != validLogLevels.cend()) {
            logLevel = logLevelIt->second;
        }
        else {
            std::cerr << "Invalid log level \"" << rumba::RUMBA_LOG_LEVEL
                      << "\", defaulting back to LogLevel::ERROR" << std::endl;
            logLevel = buildboxcommon::LogLevel::ERROR;
        }
    }

    auto &logger = buildboxcommon::logging::Logger::getLoggerInstance();
    if (!logDirectory.empty()) {
        buildboxcommon::FileUtils::createDirectory(logDirectory.c_str());
        logger.setOutputDirectory(logDirectory.c_str());
        // (The destination needs to be changed before initializing the
        // logger.)

        // Disable logging to stderr if using logfiles
        logger.disableStderr();
    }
    logger.initialize(programName);

    BUILDBOX_LOG_SET_LEVEL(logLevel);
}

pid_t spawnChild(char **argv)
{
    pid_t pid = fork();

    if (pid == -1) {
        // Error
        std::cerr << "rumba: failed to fork, errno = [" << errno << ":"
                  << strerror(errno) << "]" << std::endl;
        exit(1);
    }
    else if (pid > 0) {
        // Parent
        return pid;
    }
    else {
        // Child
        if (execv(argv[0], argv) == -1) {
            std::cerr << "rumba: could not execute compiler command: "
                      << strerror(errno) << std::endl;
            return errno;
        }
    }
    // Should never be reached
    exit(1);
}

bool isObjectFile(const std::string &path)
{
    const std::string suffix = ".o";
    return path.size() > suffix.size() &&
           path.substr(path.size() - suffix.size()) == suffix;
}

bool verifyOutputFile(buildboxcommon::CASClient *casClient,
                      const buildboxcommon::OutputFile &localFile,
                      const buildboxcommon::OutputFile &remoteFile)
{
    if (localFile.path() == remoteFile.path()) {
        if (localFile.digest() == remoteFile.digest()) {
            BUILDBOX_LOG_INFO("recc verify: File digest match for '"
                              << localFile.path()
                              << "': " << localFile.digest().hash() << "/"
                              << localFile.digest().size_bytes());
            return true;
        }
        else if (isObjectFile(localFile.path())) {
            const std::string stripCommand =
                buildboxcommon::SystemUtils::getPathToCommand("strip");
            if (stripCommand.empty()) {
                VERIFY_ERROR("recc verify: strip: command not found");
                return false;
            }

            buildboxcommon::TemporaryFile tempLocalFile, tempRemoteFile;
            try {
                casClient->download(tempRemoteFile.fd(), remoteFile.digest());
            }
            catch (const std::exception &e) {
                VERIFY_ERROR("recc verify: Download failed for output "
                             << remoteFile.digest().hash() << "/"
                             << remoteFile.digest().size_bytes() << ": "
                             << e.what());
                return false;
            }
            try {
                buildboxcommon::FileUtils::copyFile(localFile.path().c_str(),
                                                    tempLocalFile.name());
            }
            catch (const std::exception &e) {
                VERIFY_ERROR("recc verify: Failed to copy local output "
                             << localFile.path() << ": " << e.what());
                return false;
            }
            int stripExit = buildboxcommon::SystemUtils::executeCommandAndWait(
                {stripCommand, tempLocalFile.name(), tempRemoteFile.name()});
            if (stripExit != 0) {
                VERIFY_ERROR("recc verify: strip failed with exit code "
                             << stripExit << " for '" << localFile.path()
                             << "'");
                return false;
            }

            const auto localStrippedDigest =
                buildboxcommon::CASHash::hash(tempLocalFile.fd());
            const auto remoteStrippedDigest =
                buildboxcommon::CASHash::hash(tempRemoteFile.fd());

            if (localStrippedDigest == remoteStrippedDigest) {
                BUILDBOX_LOG_INFO("recc verify: File digest match for '"
                                  << localFile.path() << "' after stripping: "
                                  << localStrippedDigest.hash() << "/"
                                  << localStrippedDigest.size_bytes());
                return true;
            }
        }
        VERIFY_ERROR("recc verify: File digest mismatch for '"
                     << localFile.path() << "': local "
                     << localFile.digest().hash() << "/"
                     << localFile.digest().size_bytes() << ", remote "
                     << remoteFile.digest().hash() << "/"
                     << remoteFile.digest().size_bytes());
        return false;
    }
    else {
        VERIFY_ERROR("recc verify: File path mismatch: local '"
                     << localFile.path() << "', remote '" << remoteFile.path()
                     << "'");
        return false;
    }
}

int verifyRemoteBuild(int argc, char **argv,
                      build::buildbox::CompilerExecutionData *compilationData)
{
    int exitCode;

    setupSignals();

    BUILDBOX_LOG_INFO("recc verify: Local build");
    setenv("RECC_SKIP_CACHE", "1", 1);
    setenv("RECC_CACHE_ONLY", "1", 1);
    setenv("RECC_CACHE_UPLOAD_LOCAL_BUILD", "1", 1);
    setenv("RECC_ACTION_SALT", "verify:local", 1);
    recc::ExecutionContext localReccContext;
    localReccContext.setStopToken(s_signalReceived);
    try {
        exitCode = localReccContext.execute(argc, argv);
    }
    catch (const std::exception &e) {
        if (s_signalReceived) {
            std::cerr << "rumba: caught signal\n";
        }
        else {
            VERIFY_ERROR(
                "recc verify failed during local execution: " << e.what());
        }

        return 1;
    }
    setenv("RECC_SKIP_CACHE", "", 1);
    setenv("RECC_CACHE_ONLY", "", 1);
    setenv("RECC_CACHE_UPLOAD_LOCAL_BUILD", "", 1);
    if (exitCode != 0) {
        BUILDBOX_LOG_INFO("recc verify: Local build failed with exit code "
                          << exitCode);
    }

    if (localReccContext.getActionDigest().hash().empty()) {
        // No Action available, skip remote execution and verification
        BUILDBOX_LOG_INFO("recc verify: Not a compiler command");
        return exitCode;
    }

    BUILDBOX_LOG_INFO("recc verify: Remote execution");
    // We use output from local compilation, no need to download outputs
    setenv("RECC_DONT_SAVE_OUTPUT", "1", 1);
    setenv("RECC_ACTION_SALT", "verify:remote", 1);
    recc::ExecutionContext remoteReccContext;
    remoteReccContext.setStopToken(s_signalReceived);
    try {
        remoteReccContext.execute(argc, argv);
    }
    catch (const std::exception &e) {
        if (s_signalReceived) {
            std::cerr << "rumba: caught signal\n";
        }
        else {
            VERIFY_ERROR(
                "recc verify failed during remote execution: " << e.what());
        }

        return 1;
    }

    auto reccData = compilationData->mutable_recc_data();

    reccData->mutable_action_digest()->CopyFrom(
        localReccContext.getActionDigest());

    BUILDBOX_LOG_INFO("recc verify: Local action digest "
                      << localReccContext.getActionDigest().hash() << "/"
                      << localReccContext.getActionDigest().size_bytes()
                      << ", remote action digest "
                      << remoteReccContext.getActionDigest().hash() << "/"
                      << remoteReccContext.getActionDigest().size_bytes());

    const auto &localResult = localReccContext.getActionResult();
    const auto &remoteResult = remoteReccContext.getActionResult();

    if (localResult.exit_code() != remoteResult.exit_code()) {
        VERIFY_ERROR("recc verify: Exit code mismatch: local "
                     << localResult.exit_code() << ", remote "
                     << remoteResult.exit_code());
        return 1;
    }

    buildboxcommon::CASClient *casClient = remoteReccContext.getCasClient();

    if (localResult.output_files().size() ==
        remoteResult.output_files().size()) {
        for (int i = 0; i < localResult.output_files().size(); i++) {
            if (!verifyOutputFile(casClient, localResult.output_files(i),
                                  remoteResult.output_files(i))) {
                exitCode = 1;
            }
        }
    }
    else {
        VERIFY_ERROR("recc verify: Different number of output files");
        exitCode = 1;
    }

    if (localResult.output_directories().size() ==
        remoteResult.output_directories().size()) {
        for (int i = 0; i < localResult.output_directories().size(); i++) {
            const auto &localDirectory = localResult.output_directories(i);
            const auto &remoteDirectory = remoteResult.output_directories(i);
            if (localDirectory.path() == remoteDirectory.path()) {
                if (localDirectory.tree_digest() ==
                    remoteDirectory.tree_digest()) {
                    BUILDBOX_LOG_INFO(
                        "recc verify: Directory digest match for '"
                        << localDirectory.path()
                        << "': " << localDirectory.tree_digest().hash() << "/"
                        << localDirectory.tree_digest().size_bytes());
                }
                else {
                    VERIFY_ERROR(
                        "recc verify: Directory digest mismatch for '"
                        << localDirectory.path() << "': local "
                        << localDirectory.tree_digest().hash() << "/"
                        << localDirectory.tree_digest().size_bytes()
                        << ", remote " << remoteDirectory.tree_digest().hash()
                        << "/" << remoteDirectory.tree_digest().size_bytes());
                    exitCode = 1;
                }
            }
            else {
                VERIFY_ERROR("recc verify: Directory path mismatch: local '"
                             << localDirectory.path() << "', remote '"
                             << remoteDirectory.path() << "'");
                exitCode = 1;
            }
        }
    }
    else {
        VERIFY_ERROR("recc verify: Different number of output directories");
        exitCode = 1;
    }

    return exitCode;
}
} // namespace

int main(int argc, char **argv)
{
#ifdef RUMBA_CONFIG_DIR
    std::string RUMBA_CONFIG = std::string(RUMBA_CONFIG_DIR);
#else
    std::string RUMBA_CONFIG = std::string("");
#endif

    parseConfig(RUMBA_CONFIG);
    setupLoggerFromEnvironment("rumba");

    if (argc < 2) {
        std::cerr << "USAGE: rumba <command>" << std::endl;
        return 1;
    }

    // recc needs a full path to the executable
    const std::string unresolvedPathToCommand = argv[1];
    std::string resolvedPathToCommand(unresolvedPathToCommand);
    if (unresolvedPathToCommand.find('/') == std::string::npos) {
        resolvedPathToCommand = buildboxcommon::SystemUtils::getPathToCommand(
            unresolvedPathToCommand);
        if (resolvedPathToCommand.empty()) {
            BUILDBOX_LOG_ERROR("Unable to resolve path to executable=\""
                               << unresolvedPathToCommand << "\"");
            return 1;
        }

        BUILDBOX_LOG_INFO("Resolved \"" << argv[1] << "\" to \""
                                        << resolvedPathToCommand << "\"");
        argv[1] = &resolvedPathToCommand[0];
    }

    const bool verify = rumba::RUMBA_USE_RECC && rumba::RUMBA_VERIFY;

    pid_t pid = -1;
    if (!rumba::RUMBA_USE_RECC) {
        pid = spawnChild(&argv[1]);
    }

    // Start gathering data while we're waiting for compiliation to finish
    build::buildbox::CompilerExecutionData compilationData;

    DataUtils::collectCompilationData(argc, argv, unresolvedPathToCommand,
                                      compilationData);

    int exit_code;

    if (!rumba::RUMBA_USE_RECC) {
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status)) {
            exit_code = WEXITSTATUS(status);
        }
        else {
            BUILDBOX_LOG_ERROR(
                "compiler child process was "
                << "signaled or otherwise did not exit normally");
            exit_code = 1;
        }
    }
    else if (verify) {
        exit_code = verifyRemoteBuild(argc - 1, &argv[1], &compilationData);
    }
    else {
        recc::ExecutionContext recc_context;

        setupSignals();
        recc_context.setStopToken(s_signalReceived);

        try {
            exit_code = recc_context.execute(argc - 1, &argv[1]);
        }
        catch (const std::exception &e) {
            if (s_signalReceived) {
                std::cerr << "rumba: caught signal\n";
                return 1;
            }

            BUILDBOX_LOG_ERROR(
                "recc failed, falling back to local execution: " << e.what());

            // Re-exec the rumba process without recc to ensure clean
            // initialization.
            unsetenv("RUMBA_USE_RECC");
            rumba::RUMBA_USE_RECC = false;

            execv(argv[0], argv);

            BUILDBOX_LOG_ERROR("Failed to re-exec "
                               << argv[0]
                               << " after recc failure: " << strerror(errno));
            // POSIX defines exit code 127 for errors preventing execution
            return 127;
        }

        auto reccData = compilationData.mutable_recc_data();

        const auto durationMetrics = recc_context.getDurationMetrics();
        for (auto const &iter : *durationMetrics) {
            (*reccData->mutable_duration_metrics())[iter.first] =
                google::protobuf::util::TimeUtil::MicrosecondsToDuration(
                    iter.second.value().count());
        }

        const auto counterMetrics = recc_context.getCounterMetrics();
        for (auto const &iter : *counterMetrics) {
            (*reccData->mutable_counter_metrics())[iter.first] = iter.second;
        }

        reccData->mutable_action_digest()->CopyFrom(
            recc_context.getActionDigest());
    }
    // record elapsed time since timestamp value is record
    *compilationData.mutable_duration() =
        buildboxcommon::TimeUtils::now() - compilationData.timestamp();

    *compilationData.mutable_local_resource_usage() =
        buildboxcommon::ExecutionStatsUtils::getChildrenProcessRusage();

    // Send the proto
    DataUtils::sendData(compilationData);

    return exit_code;
}
