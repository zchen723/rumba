// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define TESTING_RUMBAD_BUFFEREDPUBLISHER
// component under test
#include <rumbad_bufferedpublisher.h>

#include <rumbad_stdoutwriter.h>

// third party includes
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace rumbad {
// NOTE: We're not using the test namespace here because we need access to
// the private members of BufferedPublisher.

using namespace ::testing;

const int TEST_BATCH_SIZE = 10;
const std::chrono::seconds TEST_PUBLISH_INTERVAL(1);

TEST(BufferedPublisherTest, TestBuffering)
{
    StdoutWriter writer;
    BufferedPublisher publisher(TEST_BATCH_SIZE, TEST_PUBLISH_INTERVAL,
                                &writer);
    publisher.doPublish = false; // Disable the publishing thread for this test

    ASSERT_EQ(publisher.buffer.size(), 0);

    build::buildbox::CompilerExecutionData compilationData;
    *compilationData.mutable_command() = "test command";
    publisher.publish(compilationData.SerializeAsString());

    ASSERT_EQ(publisher.buffer.size(), 1);
}

TEST(BufferedPublisherTest, TestBatching)
{
    const int TEST_MESSAGE_COUNT =
        15; // Ensure TEST_BATCH_SIZE < TEST_MESSAGE_SIZE < 2 * TEST_BATCH_SIZE

    StdoutWriter writer;
    BufferedPublisher publisher(TEST_BATCH_SIZE, TEST_PUBLISH_INTERVAL,
                                &writer);
    publisher.doPublish = false; // Disable the publishing thread for this test

    ASSERT_EQ(publisher.buffer.size(), 0);

    // Publish a test message a number of times
    build::buildbox::CompilerExecutionData compilationData;
    *compilationData.mutable_command() = "test command";
    for (int i = 0; i < TEST_MESSAGE_COUNT; i++) {
        publisher.publish(compilationData.SerializeAsString());
    }

    // Now trigger the final publishing of a batch and make sure the right
    // number of messages were removed from the buffer
    ASSERT_EQ(publisher.buffer.size(), TEST_MESSAGE_COUNT);
    publisher.publishBatch();
    ASSERT_EQ(publisher.buffer.size(), TEST_MESSAGE_COUNT - TEST_BATCH_SIZE);

    // Trigger another batch publishing; this time without a full batch and
    // check that the buffer was fully drained
    publisher.publishBatch();
    ASSERT_EQ(publisher.buffer.size(), 0);
}

} // namespace rumbad
