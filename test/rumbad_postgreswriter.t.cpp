// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define TESTING_RUMBAD_POSTGRES_WRITER

#include <iostream>

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <pqxx/pqxx>

#include <rumbad_postgreswriter.h>
#include <rumbad_utils.h>
#include <rumbad_writer.h>

namespace rumbad {

using namespace ::testing;

class PostgresWriterTest : public ::testing::Test {
  protected:
    void SetUp() override
    {
        try {
            setenv("PGUSER", "postgres", 0);
            setenv("PGPASSWORD", "insecure", 0);
            setenv("PGHOST", "rumbad-test-db", 0);

            setenv("PGDATABASE", "template1", 1);
            pqxx::connection c;
            pqxx::nontransaction w(c);
            w.exec("DROP DATABASE IF EXISTS rumbad_testing");
            w.exec("CREATE DATABASE rumbad_testing");
            w.commit();
            c.disconnect();

            setenv("PGDATABASE", "rumbad_testing", 1);
            pqxx::connection tableConnection;
            pqxx::work tableTransaction(tableConnection);
            tableTransaction.exec("CREATE TABLE compilation_data (timestamp "
                                  "TIMESTAMPTZ, data JSONB)");
            tableTransaction.commit();
            tableConnection.disconnect();
        }
        catch (const std::exception &ex) {
            std::cout << "Failed to create test database. " << ex.what();
            GTEST_SKIP() << "Failed to create test database. " << ex.what()
                         << "Skipping PostgresWriter tests";
        }
    }

    void TearDown() override
    {
        try {
            setenv("PGDATABASE", "template1", 1);
            pqxx::connection c;
            pqxx::nontransaction w(c);
            w.exec("DROP DATABASE IF EXISTS rumbad_testing");
            w.commit();
            c.disconnect();
        }
        catch (const std::exception &ex) {
            std::cout << "WARNING: failed to drop test database on completion."
                      << std::endl;
        }
    }
};

void verifyMessageWritten(std::string command, Message message,
                          int messageCount)
{
    pqxx::connection connection;
    pqxx::work work(connection);

    pqxx::result result = work.exec(
        "SELECT timestamp, data->'command' as command FROM compilation_data");
    ASSERT_EQ(result.size(), messageCount);

    for (auto const &row : result) {
        std::string jsonCommand("\"" + command + "\"");
        ASSERT_EQ(row["command"].as<std::string>(), jsonCommand);

        std::string dbTimestamp = row["timestamp"].as<std::string>();
        std::string exampleTimestamp = formatTimestamp(message.timestamp);
        ASSERT_EQ(dbTimestamp, exampleTimestamp);
    }
}

TEST_F(PostgresWriterTest, WriterPopulatesDatabase)
{
    std::string exampleCommand("test command");
    setenv("PGDATABASE", "rumbad_testing", 1);
    PostgresWriter writer;

    // Generate some fake compilation data
    build::buildbox::CompilerExecutionData compilationData;
    *compilationData.mutable_command() = exampleCommand;
    Message example;
    example.data = compilationData;
    example.timestamp = std::chrono::system_clock::now();

    // Let the writer write the example to the database
    std::vector<Message> messages{example};
    writer.write(messages);

    // Check that the message ended up in the database as expected
    verifyMessageWritten(exampleCommand, example, 1);
}

TEST_F(PostgresWriterTest, WriterConnectionRefreshing)
{
    std::string exampleCommand("test command");
    setenv("PGDATABASE", "rumbad_testing", 1);
    char *testHost = getenv("PGHOST");
    setenv("PGHOST", "notahost", 1);
    PostgresWriter writer;

    // Generate some fake compilation data
    build::buildbox::CompilerExecutionData compilationData;
    *compilationData.mutable_command() = exampleCommand;
    Message example;
    example.data = compilationData;
    example.timestamp = std::chrono::system_clock::now();

    // Try to write to the non-existent database. This should throw
    // on account of not having a usable connection, and set the flag
    // which triggers connection refreshing on the next attempt.
    std::vector<Message> messages{example};
    EXPECT_THROW(writer.write(messages), pqxx::broken_connection);

    // Now set the connection info properly and try again, this should
    // work thanks to the connection being refreshed.
    setenv("PGHOST", testHost, 1);
    EXPECT_NO_THROW(writer.write(messages));

    // Check that the message ended up in the database as expected
    verifyMessageWritten(exampleCommand, example, 1);
}

TEST_F(PostgresWriterTest, WriterHealthyConnectionRetained)
{
    std::string exampleCommand("test command");
    setenv("PGDATABASE", "rumbad_testing", 1);

    // Initialize the PostgresWriter with correct connection info
    PostgresWriter writer;

    // Generate some fake compilation data
    build::buildbox::CompilerExecutionData compilationData;
    *compilationData.mutable_command() = exampleCommand;
    Message example;
    example.data = compilationData;
    example.timestamp = std::chrono::system_clock::now();

    // Break the connection info, causing the writer to throw if it tries
    // to use a new connection
    char *testHost = getenv("PGHOST");
    setenv("PGHOST", "notahost", 1);

    // Write the message to the database, which should reuse the existing
    // and correct connection
    std::vector<Message> messages{example};
    EXPECT_NO_THROW(writer.write(messages));

    // Write the message a second time, which should also reuse the existing
    // and correct connection
    EXPECT_NO_THROW(writer.write(messages));

    // Check that the message ended up in the database as expected
    setenv("PGHOST", testHost, 1);
    verifyMessageWritten(exampleCommand, example, 2);
}

} // namespace rumbad
