FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

RUN apt-get update && apt-get install -y libpqxx-dev jq

RUN git clone https://gitlab.com/BuildGrid/recc.git --depth=1 && \
    cd recc && mkdir build && cd build && cmake .. && make -j$(nproc) && make install

ARG EXTRA_CMAKE_FLAGS=
ARG BUILD_TESTS=OFF

ENV BUILDBOX_COMMON_SOURCE_ROOT=/buildbox-common
ENV CMAKE_OPTS="DCMAKE_BUILD_TYPE=DEBUG"

COPY . /rumba

RUN cd /rumba && mkdir -p build && cd build && \
    cmake ${CMAKE_OPTS} -DBUILD_TESTING=${BUILD_TESTS} "${EXTRA_CMAKE_FLAGS}" .. && make -j$(nproc)

ENV PATH "/rumba/build/rumba/:/rumba/build/rumbad/:$PATH"
