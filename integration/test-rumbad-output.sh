#!/usr/bin/env bash

# Issues a simple command using rumba and verify rumbad publishes
# the expected output to it's stdout file. This will at a minumum
# check that all the expected keys are there, and individual keys
# may have stricter validations
#
# This test uses the same hello.cpp used in test-recc-output.sh

# Startup rumba logging to a file for easy inspection
RUMBAD_OUTPUT_FILE="/tmp/rumbad.out"
RUMBAD_PUBLISH_INTERVAL=1 rumbad 19111 > ${RUMBAD_OUTPUT_FILE} &
sleep 1

gcc_path=$(which gcc)
export TEST_ENVIRONMENT_VARIABLE="EXPECTED_VALUE"

# Run a compilation with rumba and verify that the output is correct
$(which rumba) gcc -I"$(pwd)" -static hello.cpp -o hello
if [[ "$(./hello)" = "hello_world" ]];
then
    echo "Correct output in binary compiled using rumba"
else
    echo "Incorrect output in binary compiled using rumba"
    exit 1
fi

sleep 2
# Extract the JSON part of the data written to the file
RUMBAD_JSON=$(cat ${RUMBAD_OUTPUT_FILE})

if [ -z "${RUMBAD_JSON}" ];
then
    echo "Unable to extract JSON from ${RUMBAD_OUTPUT_FILE}"
    exit 1;
fi

echo "Found the following JSON, verifiying fields..."
echo "${RUMBAD_JSON}" | jq -r '.'

# Associative array of json fields in terms of the jq query to get to them and their expected values
declare -A rumbaFieldValues
rumbaFieldValues+=(
    [".command"]="${gcc_path}"
    [".fullCommand"]="gcc -I$(pwd) -static hello.cpp -o hello"
    [".environmentVariables.TEST_ENVIRONMENT_VARIABLE"]="EXPECTED_VALUE"
    [".sourceFileInfo[0].name"]="hello.cpp"
    [".sourceFileInfo[0].digest.sizeBytes"]="$(stat --printf='%s' hello.cpp)"
    [".sourceFileInfo[0].digest.hash"]=$(sha256sum hello.cpp | cut -d' ' -f1)
    [".workingDirectory"]="$(pwd)"
    [".platform.properties[0].name"]="ISA"
    [".platform.properties[1].name"]="OSFamily"
    [".toolName"]="" # Added so that this test fails when field is used
    [".toolVersion"]="" # Added so that this test fails when this field is used
)
# Verify that all the fields specified in rumbaFieldValues
# exist and have the specified value
for key in "${!rumbaFieldValues[@]}";
do
    foundKey=$(echo "${RUMBAD_JSON}" | jq -r "${key}")
    if [[ ${foundKey} == "null" ]];
    then
        echo "No value was found for key ${key} in rumbad json"
        echo "Expected to find: ${rumbaFieldValues[${key}]}"
        exit 1
    elif [[ ${foundKey} != "${rumbaFieldValues[${key}]}" ]];
    then
        echo "Incorrect value for key ${key} in rumbad json"
        echo "Expected to find: ${rumbaFieldValues[${key}]}"
        exit 1
    fi
done

# Array of fields which should exist (not null)
declare -a rumbaNotNullFields
rumbaNotNullFields+=(
    ".localResourceUsage.commandRusage.utime"
    ".localResourceUsage.commandRusage.stime"
    ".localResourceUsage.commandRusage.maxrss"
    ".localResourceUsage.commandRusage.minflt"
    ".localResourceUsage.commandRusage.majflt"
    ".localResourceUsage.commandRusage.inblock"
    ".localResourceUsage.commandRusage.oublock"
    ".localResourceUsage.commandRusage.nvcsw"
    ".localResourceUsage.commandRusage.nivcsw"
)
# Verify that all the fields in rumbaNotNullFields are not null
for key in "${rumbaNotNullFields[@]}";
do
    foundKey=$(echo "${RUMBAD_JSON}" | jq -r "${key}")
    if [[ ${foundKey} == "null" ]];
    then
        echo "No value was found for key ${key} in rumbad json"
        exit 1
    fi
done

# Check that correlatedInvocationId is generated and is a valid UUID4
invocationId=$(echo "${RUMBAD_JSON}" | jq -r '.correlatedInvocationsId')
if [[ ! "${invocationId}" =~ [a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12} ]];
then
    echo "Found value for generated correlatedInvocationsId [${invocationId}] doesn't match UUID4 regex"
    exit 1
fi

# Validate the timestamp
timestamp=$(echo "${RUMBAD_JSON}" | jq -r '.timestamp')
if [[ -z "$timestamp" || ! $(date --date="$timestamp" --iso-8601="ns") ]]
then
    echo "Invalid value for timestamp [${timestamp}]"
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
