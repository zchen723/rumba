// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <google/protobuf/util/json_util.h>
#include <pqxx/pqxx>
#include <pqxx/tablewriter>

#include <build/buildbox/local_execution.pb.h>
#include <buildboxcommon_logging.h>

#include <rumbad_postgreswriter.h>
#include <rumbad_utils.h>

namespace rumbad {

PostgresWriter::PostgresWriter()
{
    try {
        this->connection = std::make_unique<pqxx::connection>();
        this->connectionHealthy = true;
    }
    catch (const pqxx::broken_connection &ex) {
        // Couldn't connect immediately, mark the connection as broken
        // so that we refresh it before we try to use it.
        this->connectionHealthy = false;
    }
}

void PostgresWriter::write(const std::vector<Message> &messages)
{
    try {
        if (!this->connectionHealthy) {
            // Try to refresh the connection. This might throw a new
            // pqxx::broken_connection if the problem hasn't gone away.
            this->connection = std::make_unique<pqxx::connection>();
            this->connectionHealthy = true;
        }
        pqxx::work transaction(*this->connection, "CompilationDataBatchWrite");

        std::vector<std::string> columnNames{"timestamp", "data"};
        pqxx::tablewriter tablewriter(transaction, "compilation_data",
                                      columnNames.begin(), columnNames.end());

        google::protobuf::util::JsonPrintOptions options;
        options.always_print_primitive_fields = true;
        for (int i = 0; i < messages.size(); i++) {
            std::string json;
            google::protobuf::util::MessageToJsonString(messages[i].data,
                                                        &json, options);
            std::vector<std::string> record{
                formatTimestamp(messages[i].timestamp), json};

            tablewriter << record;
        }

        tablewriter.complete();
        transaction.commit();
    }
    catch (const pqxx::broken_connection &ex) {
        // Mark the connection as broken so that the next call will
        // attempt to refresh it.
        this->connectionHealthy = false;

        // Rethrow here even if we made a new connection, to handle
        // retries using the caller's logic.
        throw;
    }
}

} // namespace rumbad
