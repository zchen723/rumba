// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <memory>

#include <google/protobuf/util/json_util.h>

#include <buildboxcommon_logging.h>
#include <rumbad_teewriter.h>
#include <rumbad_utils.h>

namespace rumbad {

TeeWriter::TeeWriter()
{
    this->stdoutWriter = std::make_unique<StdoutWriter>();
    this->postgresWriter = std::make_unique<PostgresWriter>();
}

void TeeWriter::write(const std::vector<Message> &messages)
{
    this->stdoutWriter->write(messages);
    this->postgresWriter->write(messages);
}

} // namespace rumbad
